var async = require("async");
var exec = require("child_process").exec;
var spawn = require('child_process').spawn;
var fs = require("fs");
var data = require('./data.json');
var utils = require('./utils.js');
var additionFuncs = require('./additionFuncs');

var defaults = data.defaults;
var steps = data.steps;
var funcs = [];

var schemaName,
	username,
	stepNumber,
	importDB,
	iFrom = 0,
	iTo,
	dbName = 'db.sql',
	port = 8080,
	tomcatPort;

function createExecFunc(i) {
    return function(callback) { 
    	var cmd, options;
    	if (steps[i].step.before) console.log(steps[i].step.before.replace('[PORT]', port));
	    if (steps[i].step.options){
	    	options = {cwd: steps[i].step.options}
	    	cmd = exec(steps[i].step.cmd.replace('[PORT]', port), options);
	    } else {
	    	cmd = exec(steps[i].step.cmd.replace('[PORT]', port));
	    }
	    
	    cmd.stdout.on('data', function (data) {
		  	if (data){
				console.log(data);
				fs.appendFileSync('data.log', data);
			}
		});

		cmd.stderr.on('data', function (data) {
		  	console.log('err: ' + data);
		  	fs.appendFileSync('error.log', data);
		});

		cmd.on('close', function (code) {
			if (steps[i].step.after)
				console.log(steps[i].step.after.replace('[PORT]', port) +  'with code: ' + code);
			else
				console.log('Finish step with code: ' + code);
			fs.appendFileSync('data.log', 'Finish step with code: ' + code + '\n--------------------------------------------\n\n');
			setTimeout(function(){callback(null);}, 3000);
		});	
    };
}

function additionalFunc(i){
	return function(callback){
		if (steps[i].step.before) console.log(steps[i].step.before);
		eval('additionFuncs.' + steps[i].step.name)(function(){
														if (steps[i].step.after) console.log(steps[i].step.after);
														callback(null)
													});

	}
}

function createCopyFunc(i) {
    return function(callback) { 
    	var src = steps[i].step.src.replace("[USERNAME]", username);
    	var dest = steps[i].step.dest.replace("[USERNAME]", username);
    	if (steps[i].step.before) console.log(steps[i].step.before);
	    utils.copyFileContent(src, dest, schemaName, port, tomcatPort);
	    //if (steps[i].step.before) console.log(steps[i].step.before);
	    callback(null);
    };
}

function createSchemaAndImportDB(i) {
	
    return function(callback) { 
    	console.log('Creating Schema...');
		fs.writeFileSync('createSchema.sql', 'CREATE SCHEMA IF NOT EXISTS `' + schemaName + '` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;');
		exec(steps[i].step.cmd, function(err, stdout){
			if (err){
				throw err;
			}
			console.log("Schema was created!\n");
			console.log('Importing DB...');
			exec(importDB, function(err, stdout){
				if (err){
					throw err;
			}
			console.log("DB was imported!\n");
			callback(null);
			});
		});
	}
}

module.exports.getFunctions = function(){
	for (var i = iFrom; i <= iTo; i++) {
		switch (steps[i].step.type) {
			case "copy":
				funcs.push(createCopyFunc(i));
				break;
			case "schema":
				funcs.push(createSchemaAndImportDB(i));
				break;
			case "exec":
				funcs.push(createExecFunc(i));
				break;
			case "func":
				funcs.push(additionalFunc(i));
				break;
			default:
				break;
		}
	}
	return funcs;
}

exports.checkParams = function () {
	if (utils.grab('--help') === 'help'){
		utils.printHelp();
		process.exit();
	}
	var debug = utils.grab('--debug');
	
	username = utils.grab('--whoami');
	if (!username && !debug){
		console.log('Please enter WHOAMI param !!');
		return false;
	}
	schemaName = !utils.grab('--schema') ? defaults.schema : utils.grab('--schema');
	iFrom = !utils.grab('--from') ? 0 : utils.grab('--from') - 1;
	iTo = !utils.grab('--to') ? steps.length-1 : utils.grab('--to') - 1;
	dbName = !utils.grab('--dbname') ? dbName : utils.grab('--dbname');
	importDB = 'mysql -u root ' + schemaName + ' < ' + dbName;
	port = !utils.grab('--port') ? port : utils.grab('--port');
	tomcatPort = port - 70;

	return true;
}
