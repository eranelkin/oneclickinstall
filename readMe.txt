 How to install:
--------------
 1. download this file to your installation folder 2. extract it and copy all the files to this installation folder. (the zip folder and file can be deleted) 3. copy dump file to this folder and and rename it : db.sql 4. open db.sql file in an editor and make sure that this sql script does not contain 'create schema....', if it does, remark it. 5. open data.json file and change eran.elkin username to yours bitbucket user. 6. open terminal and type: whoami and remember your computer user. 7. open MySqlWorkBench and make sure that your new schema name does not exist. 8. open readme.txt and use this command and modify it with the new params. 

Basic Usage:
-----------
node install.js --schema [SCHEMA_NAME] --whoami [COMP_NAME] --port [PORT]

example:
-------
node install.js --schema wpl_licensee17_adm --whoami eran.elkin --port 8095

Mandatory Params:
----------------

--schema    [SCHEMA_NAME]
--whoami    [WHOAMI - terminal command]


Additional Params (Optional):
----------------------------

--port    [Port number, default is 8080]
--from    [Which Step to begin the run]
--To      [Which step to end the run]
--dbname  [Database name, default is db.sql]