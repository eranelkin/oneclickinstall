var fs = require("fs");

exports.grab = function(flag){
  var index = process.argv.indexOf(flag);
  if (index === -1){
    return null;
  }
  else {
      if (flag === '--help'){
        return 'help';
      }
      else if (flag === '--debug'){
        return 'debug';
      }
      else {
        return process.argv[index+1];
      }
  }
  //return (index === -1) ? null : (flag === '--help' ? 'help' : process.argv[index+1]);
}

function getFileContent(srcPath, callback) { 
    fs.readFile(srcPath, 'utf8', function (err, data) {
        if (err) throw err;
        callback(data);
    });
}

exports.copyFileContent = function(srcPath, savPath, strSchema, strPort, strTomcatPort) { 
    getFileContent(srcPath, function(data) {
        console.log(strSchema + strPort);
        if (strSchema !== '') fs.writeFileSync (savPath, data.replace('[SCHEMA_NAME]', strSchema)
                .replace('[NEW_PORT]',strPort).replace('[NEW_PORT]',strPort).replace('[TOMCAT_PORT]',strTomcatPort));
    });
}

exports.printHelp = function(){
  var str = 'Usage: node FILENAME.js --whoami [WHOAMI] --schema [SCHEMA_NAME] (optional)\n\n'; 
  str += 'Params:\n--whoami [computers name] >> Mandatory\n';
  str += '--schema [schema name] >> Mandatory\n';
  str += '--dbname [database name] >> Optional (default: db.sql)';
  str += '--from [START STEP] >> Optional';
  str += '--to [END STEP] >> Optional';

  console.log(str);
}
